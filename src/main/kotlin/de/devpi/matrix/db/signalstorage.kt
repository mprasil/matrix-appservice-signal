/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix.db

import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.jetbrains.exposed.dao.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.*
import org.whispersystems.libsignal.*
import org.whispersystems.libsignal.state.*
import org.whispersystems.signalservice.api.util.CredentialsProvider
import org.whispersystems.signalservice.internal.util.*
import java.security.Security
import javax.crypto.*
import javax.crypto.spec.*
import javax.crypto.spec.IvParameterSpec


class SignalStorage(id : EntityID<Long>) : LongEntity(id), SignalProtocolStore
{
	companion object : LongEntityClass<SignalStorage>(SignalStorages)
	{
		init
		{
			Security.addProvider(BouncyCastleProvider())
		}
		
		val logger : Logger = LoggerFactory.getLogger(SignalStorage::class.java)
	}
	
	var bridgeUser by BridgeUser referencedOn SignalStorages.bridgeUser
	var registrationId by SignalStorages.registrationId
	
	private var encrypted by SignalStorages.encrypted
	private var salt by SignalStorages.salt
	private var iv by SignalStorages.iv
	
	private var password = ""
	private val decryptCipher : Cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "BC")
	private val encryptCipher : Cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "BC")
	
	private var idKey by SignalStorages.identityKeyPair
	private var signalUser by SignalStorages.signalUser
	private var signalingKey by SignalStorages.signalingKey
	private var signalPassword by SignalStorages.signalPassword
	var nextPreKey by SignalStorages.nextPreKey
	var nextSignedPreKey by SignalStorages.nextSignedPreKey
	
	val number by lazy {
		return@lazy credentials.user.substringBefore(".")
	}
	
	val deviceid by lazy {
		return@lazy credentials.user.substringAfter(".").toInt()
	}
	
	var credentials : CredentialsProvider
		get()
		{
			val password = decryptStringValue(signalPassword)
			val user = decryptStringValue(signalUser)
			val key = decryptStringValue(signalingKey)
			return StaticCredentialsProvider(user, password, key)
		}
		set(value)
		{
			signalUser = encryptValue(value.user)
			signalPassword = encryptValue(value.password)
			signalingKey = encryptValue(value.signalingKey)
		}
	var identityKey : IdentityKeyPair
		get() = IdentityKeyPair(decryptValue(idKey))
		set(value)
		{
			idKey = encryptValue(value.serialize())
		}
	
	fun changeEncryption(password : String) : Boolean
	{
		if (this.password != password)
		{
			this.password = password
			if (password == "")
			{
				transaction {
					reassignEncrypted()
					encrypted = false
				}
			}
			else
			{
				transaction {
					val factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256", "BC")
					val spec = PBEKeySpec(password.toCharArray(), Base64.decode(salt), 65536, 256)
					val tmp = factory.generateSecret(spec)
					val keySpec = SecretKeySpec(tmp.encoded, "AES")
					encryptCipher.init(Cipher.ENCRYPT_MODE, keySpec)
					val newParams : ByteArray = encryptCipher.parameters.getParameterSpec(
							IvParameterSpec::class.java).iv
					if (encrypted)
					{
						val oldParams = Base64.decode(iv)
						decryptCipher.init(Cipher.DECRYPT_MODE, keySpec, IvParameterSpec(oldParams))
					}
					reassignEncrypted()
					iv = Base64.encodeBytes(newParams)
					encrypted = true
					decryptCipher.init(Cipher.DECRYPT_MODE, keySpec, IvParameterSpec(newParams))
				}
			}
			return true
		}
		return false
	}
	
	private fun reassignEncrypted()
	{
		credentials = credentials
		identityKey = identityKey
		SignalPreKey.find { SignalPreKeys.storage eq id }.forEach {
			if (it.signed)
				it.signedKey = it.signedKey
			else
				it.key = it.key
		}
		SignalSession.find { SignalSessions.storage eq id }.forEach {
			it.session = it.session
		}
	}
	
	private fun decryptStringValue(enc : String) : String = String(decryptValue(enc), Charsets.UTF_8)
	
	internal fun decryptValue(enc : String) : ByteArray
	{
		val bytes : ByteArray = Base64.decode(enc)
		if (encrypted)
		{
			if (password != "")
			{
				return decryptCipher.doFinal(bytes)
			}
			else
				throw IllegalStateException("Information is encrypted but no password is provided")
		}
		return bytes
	}
	
	private fun encryptValue(value : String) : String = encryptValue(value.toByteArray(Charsets.UTF_8))
	
	internal fun encryptValue(value : ByteArray) : String
	{
		var bytes = value
		if (password != "")
			bytes = encryptCipher.doFinal(bytes)
		return Base64.encodeBytes(bytes)
	}

// ### from IdentityKeyStore iface #################################################################################
	
	override fun getIdentityKeyPair() : IdentityKeyPair
			= identityKey
	
	override fun saveIdentity(address : SignalProtocolAddress, identityKey : IdentityKey) : Boolean
	{
		// address is the remote users address of who we should store the key
		// not necessary to encrypt, these are just public keys
		transaction {
			SignalIdentityKey.find {
				(SignalIdentityKeys.storage eq id) and
						(SignalIdentityKeys.name eq address.name) and
						(SignalIdentityKeys.deviceId eq address.deviceId)
			}.updateOrNew {
				if (it)
				{
					this.storage = this@SignalStorage
					this.address = address
				}
				this.key = identityKey
			}
		}
		return true
	}
	
	override fun isTrustedIdentity(address : SignalProtocolAddress, identityKey : IdentityKey,
								   direction : IdentityKeyStore.Direction) : Boolean
	{
		return transaction {
			val dao = SignalIdentityKey.find {
				(SignalIdentityKeys.storage eq id) and
						(SignalIdentityKeys.name eq address.name) and
						(SignalIdentityKeys.deviceId eq address.deviceId)
			}.firstOrNull()
			val trusted = dao == null || dao.key == identityKey
			SignalStorage.logger.info("isTrustedIdentity: $trusted")
			return@transaction trusted
		}
	}
	
	override fun getLocalRegistrationId() : Int
			= registrationId

// ### from SignedPreKeyStore ######################################################################################
	
	override fun loadSignedPreKey(signedPreKeyId : Int) : SignedPreKeyRecord?
	{
		logger.info("loadSignedPreKey: $signedPreKeyId")
		return transaction {
			SignalPreKey.find {
				(SignalPreKeys.storage eq id) and
						(SignalPreKeys.signed eq true) and
						(SignalPreKeys.keyId eq signedPreKeyId)
			}.firstOrNull()?.signedKey
		}
	}
	
	override fun loadSignedPreKeys() : MutableList<SignedPreKeyRecord>
	{
		logger.info("loadSignedPreKeys")
		return transaction {
			SignalPreKey.find {
				(SignalPreKeys.storage eq id) and
						(SignalPreKeys.signed eq true)
			}.map { it.signedKey }
		}.toMutableList()
	}
	
	override fun containsSignedPreKey(signedPreKeyId : Int) : Boolean
	{
		logger.info("containsSignedPreKey: $signedPreKeyId")
		return loadSignedPreKey(signedPreKeyId) != null
	}
	
	override fun storeSignedPreKey(signedPreKeyId : Int, record : SignedPreKeyRecord)
	{
		logger.info("storeSignedPreKey: $signedPreKeyId")
		transaction {
			val dao = SignalPreKey.find {
				(SignalPreKeys.storage eq id) and
						(SignalPreKeys.signed eq true) and
						(SignalPreKeys.keyId eq signedPreKeyId)
			}.firstOrNull()
			if (dao == null)
			{
				SignalPreKey.new {
					storage = this@SignalStorage
					signed = true
					keyId = signedPreKeyId
					signedKey = record
				}
			}
			else
				dao.signedKey = record
		}
	}
	
	override fun removeSignedPreKey(signedPreKeyId : Int)
	{
		logger.info("removeSignedPreKey: $signedPreKeyId")
		transaction {
			SignalPreKeys.deleteWhere {
				(SignalPreKeys.storage eq id) and
						(SignalPreKeys.signed eq true) and
						(SignalPreKeys.keyId eq signedPreKeyId)
			}
		}
	}

// ### from PreKeyStore ############################################################################################
	
	override fun loadPreKey(preKeyId : Int) : PreKeyRecord?
	{
		logger.info("loadPreKey: $preKeyId")
		return transaction {
			SignalPreKey.find {
				(SignalPreKeys.storage eq id) and
						(SignalPreKeys.signed eq false) and
						(SignalPreKeys.keyId eq preKeyId)
			}.firstOrNull()?.key
		}
	}
	
	override fun containsPreKey(preKeyId : Int) : Boolean
	{
		logger.info("containsPreKey: $preKeyId")
		return loadPreKey(preKeyId) != null
	}
	
	override fun storePreKey(preKeyId : Int, record : PreKeyRecord)
	{
		//logger.debug("storePreKey: $preKeyId")
		transaction {
			SignalPreKey.find {
				(SignalPreKeys.storage eq id) and
						(SignalPreKeys.signed eq false) and
						(SignalPreKeys.keyId eq preKeyId)
			}.updateOrNew {
				if (it)
				{
					storage = this@SignalStorage
					signed = false
					keyId = preKeyId
				}
				key = record
			}
			
		}
	}
	
	override fun removePreKey(preKeyId : Int)
	{
		logger.info("removePreKey: $preKeyId")
		transaction {
			SignalPreKeys.deleteWhere {
				(SignalPreKeys.storage eq id) and
						(SignalPreKeys.signed eq false) and
						(SignalPreKeys.keyId eq preKeyId)
			}
		}
	}

// ### from SessionStore iface #####################################################################################
	
	override fun loadSession(address : SignalProtocolAddress) : SessionRecord
	{
		logger.info("loadSession: ${address.name},${address.deviceId}")
		val dbSession = transaction {
			SignalSession.find {
				(SignalSessions.storage eq id) and
						(SignalSessions.name eq address.name) and
						(SignalSessions.deviceId eq address.deviceId)
			}.firstOrNull()?.session
		}
		if (dbSession == null)
		{
			logger.info("loadSession: Creating new session")
			return SessionRecord()
		}
		return dbSession
	}
	
	override fun containsSession(address : SignalProtocolAddress) : Boolean
	{
		logger.info("containsSession: ${address.name},${address.deviceId}")
		return transaction {
			SignalSession.find {
				(SignalSessions.storage eq id) and
						(SignalSessions.name eq address.name) and
						(SignalSessions.deviceId eq address.deviceId)
			}.firstOrNull()
		} != null
	}
	
	override fun getSubDeviceSessions(name : String) : MutableList<Int>
	{
		logger.info("getSubDeviceSessions: $name")
		return transaction {
			SignalSession.find {
				(SignalSessions.storage eq id) and (SignalSessions.name eq name) and (SignalSessions.deviceId neq 1)
			}.map { it.address.deviceId }
		}.toMutableList()
	}
	
	override fun storeSession(address : SignalProtocolAddress, record : SessionRecord)
	{
		logger.info("storeSession: ${address.name},${address.deviceId}")
		transaction {
			val dao = SignalSession.find {
				(SignalSessions.storage eq id) and
						(SignalSessions.name eq address.name) and
						(SignalSessions.deviceId eq address.deviceId)
			}.firstOrNull()
			if (dao == null)
			{
				SignalSession.new {
					this.storage = this@SignalStorage
					this.address = address
					this.session = record
				}
			}
			else
				dao.session = record
		}
	}
	
	override fun deleteAllSessions(name : String)
	{
		logger.info("deleteAllSessions: $name")
		transaction {
			SignalSessions.deleteWhere {
				(SignalSessions.storage eq id) and (SignalSessions.name eq name)
			}
		}
	}
	
	override fun deleteSession(address : SignalProtocolAddress)
	{
		logger.info("deleteSession: ${address.name},${address.deviceId}")
		transaction {
			SignalSessions.deleteWhere {
				(SignalSessions.storage eq id) and
						(SignalSessions.name eq address.name) and
						(SignalSessions.deviceId eq address.deviceId)
			}
		}
	}
}