/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix.db

object AdminRoomStatus
{
	/** The Admin user has joined the room. */
	val JOINED = 1
	/** The Admin user has left the room. */
	val LEFT = -1
	/** The Admin user was kicked or banned from the room. */
	val DISMISSED = -2
}

object AdminRoomMemberStatus
{
	/** The member hasn't contacted the admin yet. */
	val UNREGISTERED = 0
	/** The member has issued a new registration but hasn't scanned its QR code yet. */
	val ISSUED_REGISTRATION = 1
	/** The member has received the signal authentication qr code and scanned it. */
	val REGISTERED = 2
}