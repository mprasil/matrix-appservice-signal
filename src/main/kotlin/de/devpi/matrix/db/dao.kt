/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix.db

import de.devpi.matrix.*
import de.devpi.matrix.`as`.*
import msrd0.matrix.client.*
import msrd0.matrix.client.util.toImage
import org.jetbrains.exposed.dao.*
import org.jetbrains.exposed.sql.ReferenceOption.*
import org.slf4j.*
import java.awt.image.*
import java.io.*
import java.net.URI
import java.sql.Blob
import javax.imageio.ImageIO
import javax.sql.rowset.serial.SerialBlob

private val logger : Logger = LoggerFactory.getLogger(currClassName())

// ### admin_rooms #####################################################################################################

object AdminRooms : LongIdTable(name = "admin_rooms")
{
	val roomId = text("room_id")
	val status = integer("status")
}

/**
 * Stores a room where the admin was invited to link accounts using our bridge.
 */
class AdminRoom(id : EntityID<Long>) : LongEntity(id)
{
	companion object : LongEntityClass<AdminRoom>(AdminRooms)
	
	/**
	 * The room id of this room.
	 */
	var roomId : RoomId
		get() = RoomId.fromString(roomId0)
		set(value)
		{
			roomId0 = value.toString()
		}
	private var roomId0 by AdminRooms.roomId
	
	/**
	 * The status of this room.
	 */
	var status by AdminRooms.status
}

// ### admin_room_members ##############################################################################################

object AdminRoomMembers : LongIdTable(name = "admin_room_members")
{
	val room = reference("room", AdminRooms, CASCADE)
	val userId = text("user_id")
	val status = integer("status")
}

/**
 * Stores a member in a room the admin was invited to.
 */
class AdminRoomMember(id : EntityID<Long>) : LongEntity(id)
{
	companion object : LongEntityClass<AdminRoomMember>(AdminRoomMembers)
	
	/**
	 * The room where this member is in.
	 */
	var room by AdminRoom referencedOn AdminRoomMembers.room
	
	/**
	 * The user id of this member.
	 */
	var userId : MatrixId
		get() = MatrixId.fromString(userId0)
		set(value)
		{
			userId0 = value.toString()
		}
	private var userId0 by AdminRoomMembers.userId
	
	/**
	 * The status of this member.
	 */
	var status by AdminRoomMembers.status
}

// ### as_users ########################################################################################################

object AsUsers : LongIdTable(name = "as_users")
{
	val userId = text("user_id").uniqueIndex()
	val deviceId = text("device_id")
}

/**
 * Stores a user in the namespace of the application service (usually signal_). This also applies to the admin localpart,
 * no matter whether it's inside or outside the namespace.
 */
class AsUser(id : EntityID<Long>) : LongEntity(id)
{
	companion object : LongEntityClass<AsUser>(AsUsers)
	
	/**
	 * The user id of this user.
	 */
	var userId : MatrixId
		get() = MatrixId.fromString(userId0)
		set(value)
		{
			userId0 = value.toString()
		}
	private var userId0 by AsUsers.userId
	
	/**
	 * The device id of the server for this user.
	 */
	var deviceId by AsUsers.deviceId
	
	val matrixClient : MatrixClient
		get()
		{
			val client = AsClient(userId.domain, userId.localpart, hsBaseUri = URI(Configuration.hsConfig.baseUri))
			val userData = MatrixUserData(Configuration.adminUserConfig.token, deviceId)
			client.userData = userData
			return client
		}
}

// ### as_rooms ########################################################################################################

object AsRooms : LongIdTable(name = "as_rooms")
{
	val bridgeUser = reference("bridge_user", BridgeUsers, CASCADE)
	val signalId = text("signal_id").nullable()
	val name = text("name")
	val avatar = blob("avatar").nullable()
	val matrixId = text("matrix_id").uniqueIndex()
	val matrixAlias = text("matrix_alias").nullable()
	val asUser = reference("primary_as_user", AsUsers)
}

/**
 * Stores a room that bridges a signal room to matrix.
 */
class AsRoom(id : EntityID<Long>) : LongEntity(id)
{
	companion object : LongEntityClass<AsRoom>(AsRooms)
	{
		override fun new(init : AsRoom.() -> Unit) =
				super.new { before(); init() }.apply {
					after()
				}
	}
	
	private var safe = true
	
	fun before()
	{
		safe = false
	}
	
	fun after()
	{
		val room = asUser.matrixClient.createRoomWithPowerLevels()
		matrixId0 = room.id.toString()
		room.invite(bridgeUser.userId)
		room.name = name
		val cli = asUser.matrixClient
		val av = avatar0?.toRenderedImage()
		if (av != null)
		{
			val avt = av.toAvatar(cli)
			if (avt != null)
				matrixRoom.avatar = avt
		}
		safe = true
	}
	
	/**
	 * The user that we bridge this room for.
	 */
	var bridgeUser by BridgeUser referencedOn AsRooms.bridgeUser
	
	/**
	 * Primary AsUser
	 */
	var asUser by AsUser referencedOn AsRooms.asUser
	
	/**
	 * The id of this room in signal.
	 */
	var signalId by AsRooms.signalId
	
	/**
	 * The display name of this room in signal. This may only be null if `directChat` is true, in which case the name
	 * of the room should match the only contact in this room.
	 */
	var name : String
		get() = name0
		set(value)
		{
			if (safe)
				matrixRoom.name = value
			name0 = value
		}
	
	private var name0 by AsRooms.name
	
	/**
	 * Indicates whether this room is only shared between the bridgeUser and one contact
	 */
	val directChatContact by Contact.optionalReferrersOn(Contacts.directChatRoom)
	
	/**
	 * ContactRoomAssocs for this room
	 */
	val assocs by ContactRoomAssoc referrersOn ContactRoomAssocs.room
	
	/**
	 * The avatar of this room in signal. If this is a directChat the avatar should match either the contacts avatar or its color.
	 */
	private var avatar0 by AsRooms.avatar
	
	var avatar : RenderedImage?
		get() = avatar0?.toRenderedImage()
		set(value)
		{
			if (safe)
			{
				val cli = asUser.matrixClient
				if (value != null)
				{
					val av = value.toAvatar(cli)
					if (av != null)
						matrixRoom.avatar = av
				}
			}
			avatar0 = value.toBlob()
		}
	
	/**
	 * The id of this room in matrix.
	 */
	val matrixId : RoomId
		get() = RoomId.fromString(matrixId0)
	private var matrixId0 by AsRooms.matrixId
	
	/**
	 * The alias of this room in matrix. This alias should be in the namespace of the AS.
	 */
	var matrixAlias by AsRooms.matrixAlias
	
	val matrixRoom : Room by lazy {
		asUser.matrixClient.getRoom(matrixId)
	}
	
	fun invite(contact : Contact)
	{
		matrixRoom.invite(contact.matrixId)
	}
	
	fun invite(asUser : AsUser)
	{
		matrixRoom.invite(asUser.userId)
	}
}

// ### as_transactions #################################################################################################

object AsTransactions : LongIdTable(name = "as_transactions")
{
	val txnId = long("txn_id").uniqueIndex()
	val received = datetime("received")
	val json = text("json")
}

/**
 * Stores transactions sent from the HS to the AS.
 */
class AsTransaction(id : EntityID<Long>) : LongEntity(id)
{
	companion object : LongEntityClass<AsTransaction>(AsTransactions)
	
	/**
	 * The transaction id of the transaction assigned by the HS.
	 */
	var txnId by AsTransactions.txnId
	
	/**
	 * The date and time when the event was received.
	 */
	var received by AsTransactions.received
	
	/**
	 * The json content of the transaction sent from the HS.
	 */
	var json by AsTransactions.json
}

// ### last_used_transaction_ids #######################################################################################

object LastUsedTxnIds : LongIdTable(name = "last_used_transaction_ids")
{
	val tokenSha1 = text("token_sha1").uniqueIndex()
	val lastTxnId = long("last_txn_id")
}

class LastUsedTxnId(id : EntityID<Long>) : LongEntity(id)
{
	companion object : LongEntityClass<LastUsedTxnId>(LastUsedTxnIds)
	
	var tokenSha1 by LastUsedTxnIds.tokenSha1
	var token : String
		get()
		{
			throw UnsupportedOperationException("Cannot get token from hash")
		}
		set(value)
		{
			tokenSha1 = sha1(value)
		}
	
	var lastTxnId by LastUsedTxnIds.lastTxnId
}

// ### bridge_users ####################################################################################################

object BridgeUsers : LongIdTable(name = "bridge_users")
{
	val userId = text("user_id").uniqueIndex()
	val telnumber = text("tel_number").uniqueIndex()
}

/**
 * Stores a user that uses the bridge.
 */
class BridgeUser(id : EntityID<Long>) : LongEntity(id)
{
	companion object : LongEntityClass<BridgeUser>(BridgeUsers)
	
	/**
	 * The matrix user id of the bridge user. This is outside of our namespace can be even on another HS.
	 */
	var userId : MatrixId
		get() = MatrixId.fromString(userId0)
		set(value)
		{
			userId0 = "$value"
		}
	private var userId0 by BridgeUsers.userId
	
	/**
	 * The telephone number used by the bridge user in signal.
	 */
	var number by BridgeUsers.telnumber
	
	private val selfSignalContact0 by BridgeUserSelfContact referrersOn BridgeUserSelfContacts.bridgeUser
	
	val selfSignalContact by lazy {
		selfSignalContact0.first().contact
	}
	
	/**
	 * A list of all contacts of this user.
	 */
	val contacts by Contact referrersOn Contacts.bridgeUser
	
	/**
	 * A list of all rooms that the bridge created for this user.
	 */
	val rooms by AsRoom referrersOn AsRooms.bridgeUser
}

object BridgeUserSelfContacts : LongIdTable(name = "bridge_user_self_contacts")
{
	val bridgeUser = reference("bridge_user", BridgeUsers, CASCADE).uniqueIndex()
	val contact = reference("contact", Contacts, CASCADE).uniqueIndex()
}

class BridgeUserSelfContact(id : EntityID<Long>) : LongEntity(id)
{
	companion object : LongEntityClass<BridgeUserSelfContact>(BridgeUserSelfContacts)
	
	var bridgeUser by BridgeUser referencedOn BridgeUserSelfContacts.bridgeUser
	var contact by Contact referencedOn BridgeUserSelfContacts.contact
}

// ### contacts ########################################################################################################

object Contacts : LongIdTable(name = "contacts")
{
	val bridgeUser = reference("bridge_user", BridgeUsers, CASCADE)
	val number = text("tel_number")
	val name = text("name")
	val avatar = blob("avatar").nullable()
	val matrixId = text("matrix_id").uniqueIndex()
	val directChatRoom = reference("direct_chat_room", AsRooms).nullable()
	val asUser = reference("as_user", AsUsers, CASCADE)
	
	init
	{
		uniqueIndex(bridgeUser, number)
	}
}

/**
 * Stores a contact of a bridge user.
 */
class Contact(id : EntityID<Long>) : LongEntity(id)
{
	companion object : LongEntityClass<Contact>(Contacts)
	
	/**
	 * The bridge user that has this contact.
	 */
	var bridgeUser by BridgeUser referencedOn Contacts.bridgeUser
	
	/**
	 * The telephone number of the contact that the contact uses in signal.
	 */
	var number by Contacts.number
	
	/**
	 * The display name of this contact in the contacts list of the bridge user. If it doesn't have a display name,
	 * the telephone number should be used here.
	 */
	var name : String
		get() = name0
		set(value)
		{
			asUser.matrixClient.updateDisplayname(value)
			name0 = value
		}
	private var name0 by Contacts.name
	
	/**
	 * Provides an optional reference to the direct chat room of this contact
	 */
	var directChatRoom by AsRoom optionalReferencedOn Contacts.directChatRoom
	
	/**
	 * Reference to corresponding AsUser
	 */
	var asUser by AsUser referencedOn Contacts.asUser
	
	/**
	 * Room associations for this contact
	 */
	val assocs by ContactRoomAssoc referrersOn ContactRoomAssocs.contact
	
	/**
	 * The avatar of this contact in the contacts list of the bridge user. If it doesn't have an avatar, this can be
	 * null.
	 */
	private var avatar0 : Blob? by Contacts.avatar
	
	var avatar : RenderedImage?
		get() = avatar0?.toRenderedImage()
		set(value)
		{
			val cli = asUser.matrixClient
			if (value != null)
			{
				val av = value.toAvatar(cli)
				if (av != null)
					asUser.matrixClient.updateAvatar(av)
			}
			avatar0 = value.toBlob()
		}
	
	/**
	 * The matrix id of this contact. This must be inside of our namespace, since we don't know if the user is using
	 * matrix at all.
	 */
	var matrixId : MatrixId
		get() = MatrixId.fromString(matrixId0)
		set(value)
		{
			matrixId0 = "$value"
		}
	private var matrixId0 by Contacts.matrixId
}

// ### contact_room_assoc ##############################################################################################

object ContactRoomAssocs : LongIdTable(name = "contact_room_assoc")
{
	val contact = reference("contact", Contacts, CASCADE)
	val room = reference("room", AsRooms, CASCADE)
	
	init
	{
		uniqueIndex(contact, room)
	}
}

/**
 * Association between a contact and a room.
 */
class ContactRoomAssoc(id : EntityID<Long>) : LongEntity(id)
{
	companion object : LongEntityClass<ContactRoomAssoc>(ContactRoomAssocs)
	
	/**
	 * The contact that is inside the room.
	 */
	var contact by Contact referencedOn ContactRoomAssocs.contact
	
	/**
	 * The room that the contact is in.
	 */
	var room by AsRoom referencedOn ContactRoomAssocs.room
}

@Throws(IOException::class)
fun avatarToBlob(avatar : RenderedImage?) : Blob?
{
	if (avatar == null)
		return null
	val stream = ByteArrayOutputStream()
	ImageIO.write(avatar, "PNG", stream) // TODO the image should be resized here if not done by signal
	return SerialBlob(stream.toByteArray())
}

fun RenderedImage?.toBlob() : Blob? = avatarToBlob(this)

@Throws(IOException::class)
fun Blob.toRenderedImage() : BufferedImage = ImageIO.read(binaryStream)

fun RenderedImage.toAvatar(client : MatrixClient) : Avatar?
{
	try
	{
		return Avatar.fromImage(this, client)
	}
	catch (ex : MatrixAnswerException)
	{
		logger.warn("Error while uploading avatar", ex)
		
		// if the image was too large, try scaling the image down and re-upload
		if (ex is MatrixErrorResponseException && ex.errcode == "413" && width > 96 && height > 96)
		{
			val img = BufferedImage(width/2, height/2,
					if (colorModel.hasAlpha()) BufferedImage.TYPE_INT_ARGB else BufferedImage.TYPE_INT_RGB)
			logger.info("Trying to upload avatar with size ${img.width}x${img.height} instead of ${width}x$height")
			val g = img.createGraphics()
			g.drawImage(toImage(), 0, 0, img.width, img.height, null)
			return img.toAvatar(client)
		}
		
		return null
	}
}
