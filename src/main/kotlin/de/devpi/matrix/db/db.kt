/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix.db

import com.zaxxer.hikari.*
import de.devpi.matrix.Configuration.dbConfig
import org.jetbrains.exposed.dao.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils.createMissingTablesAndColumns
import org.jetbrains.exposed.sql.transactions.*
import org.slf4j.*
import org.whispersystems.signalservice.internal.util.Base64
import java.security.SecureRandom
import java.sql.Connection
import javax.sql.DataSource
import kotlin.reflect.full.companionObjectInstance

val db = DB()
val secureRandom : SecureRandom = SecureRandom.getInstance("SHA1PRNG")

fun getRString(size : Int) : String
{
	val bytes = ByteArray(size)
	secureRandom.nextBytes(bytes)
	return Base64.encodeBytes(bytes)
}

class DB
{
	companion object
	{
		val logger : Logger = LoggerFactory.getLogger(DB::class.java)
	}
	
	var dataSource : DataSource? = null
	
	fun initDb()
	{
		val url : String
		val cfg = HikariConfig()
		when
		{
			dbConfig.type == "postgres" ->
			{
				url = "jdbc:postgresql://${dbConfig.host}:${dbConfig.port}/${dbConfig.name}"
				Class.forName("org.postgresql.Driver")
				
				cfg.username = dbConfig.user
				cfg.password = dbConfig.password
			}
			dbConfig.type == "sqlite3"  ->
			{
				url = "jdbc:sqlite:${dbConfig.file}"
				Class.forName("org.sqlite.JDBC")
				cfg.maximumPoolSize = 1
			}
			else                        -> throw IllegalArgumentException("Unknown db type: ${dbConfig.type}")
		}
		
		cfg.jdbcUrl = url
		dataSource = HikariDataSource(cfg)
		
		Database.connect(dataSource!!,
				manager = { ThreadLocalTransactionManager(it, Connection.TRANSACTION_SERIALIZABLE) })
		
		transaction {
			createMissingTablesAndColumns(
					AdminRooms,
					AdminRoomMembers,
					AsUsers,
					AsRooms,
					AsTransactions,
					LastUsedTxnIds,
					BridgeUsers,
					Contacts,
					ContactRoomAssocs,
					SignalStorages,
					SignalIdentityKeys,
					SignalPreKeys,
					SignalSessions,
					BridgeUserSelfContacts
			)
		}
		
		logger.info("DB: Connected to $url")
	}
}

fun initDb() = db.initDb()

inline fun <T> Iterable<T>.forFirst(block : T.() -> Unit)
		= this.firstOrNull()?.apply(block)


inline fun <ID : Any, reified T : Entity<ID>> Iterable<T>.getOrNew(
		crossinline update : T.() -> Unit) : T
		= this.firstOrNull().getOrNew(update)

@Suppress("UNCHECKED_CAST")
inline fun <ID : Any, reified T : Entity<ID>> T?.getOrNew(
		crossinline update : T.() -> Unit) : T
		= this.updateOrNew(true) { update() }

inline fun <ID : Any, reified T : Entity<ID>> Iterable<T>.updateOrNew(
		noUpdate : Boolean = false, crossinline block : T.(isNew : Boolean) -> Unit) : T
		= this.firstOrNull().updateOrNew(noUpdate, block)

@Suppress("UNCHECKED_CAST")
inline fun <ID : Any, reified T : Entity<ID>> T?.updateOrNew(noUpdate : Boolean = false,
															 crossinline block : T.(isNew : Boolean) -> Unit) : T
{
	var dbValue = this
	if (dbValue == null)
	{
		val obj = T::class.companionObjectInstance
		if (obj is EntityClass<*, *>)
		{
			obj as EntityClass<ID, T>
			dbValue = obj.new { (this@new as T).block(true) } as T
		}
		else
			throw IllegalStateException("Entity does not have a correct companion object of type \"EntityClass\"")
	}
	else if (!noUpdate)
		dbValue.block(false)
	return dbValue
}