/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix.`as`

import de.devpi.matrix.MextrixMatrixASSignal
import de.devpi.matrix.`as`.SignalAdminClient.adminClient
import de.devpi.matrix.`as`.SignalAdminClient.logger
import de.devpi.matrix.db.*
import msrd0.matrix.client.*
import msrd0.matrix.client.event.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.whispersystems.signalservice.api.push.SignalServiceAddress
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException
import org.whispersystems.signalservice.internal.push.SignalServiceProtos.SyncMessage.*

enum class Command(val description : String?)
{
	REGISTER("Generate a new registration QR Code"),
	SYNC("Synchronize the contacts and rooms"),
	NUKE("Delete all userdata"),
	STATUS("Show the current status"),
	HELP("Show this help"),
	SIGNALINFO("Test whether a phone number is registered with signal"),
	STARTCHAT("Start a new chat with a supplied phone number"),
	UNKNOWN(null)
}

private val commandRegex = Regex("""\A[a-z]+""", RegexOption.IGNORE_CASE)
private val phoneRegex = Regex("""^\+?[1-9]\d{1,14}$""")

fun parseCommand(cmd : String) : Command
{
	val command = commandRegex.find(cmd)?.value
	Command.values().find { it.toString().equals(command, true) }?.let { return it }
	return Command.UNKNOWN
}

fun splitArguments(maxIndex : Int, string : String) : List<String>
{
	val substrings = string.replace("\\s+".toRegex(), " ").split(" ", limit = maxIndex + 2)
	return if (substrings.size <= 1)
		emptyList()
	else
		substrings.subList(1, substrings.size)
}

/**
 * Registers this room and all current members in our database.
 */
fun registerRoom(r : Room)
{
	transaction {
		if (AdminRoom.find { AdminRooms.roomId eq r.id.toString() }.firstOrNull() != null)
			return@transaction
		
		greetRoom(r)
		
		val dao = AdminRoom.new {
			roomId = r.id
			status = AdminRoomStatus.JOINED
		}
		
		for (m in r.members)
			AdminRoomMember.new {
				room = dao
				userId = m
				status = AdminRoomMemberStatus.UNREGISTERED
			}
	}
}

/**
 * Sends a plain text message in a room, optionally adding a `user: ` in front of the message.
 */
fun sendMessage(msg : String, room : Room, user : MatrixId? = null)
{
	if (user != null)
	{
		val display : String = adminClient.displayname(user) ?: user.toString()
		val displayLink = """<a href="https://matrix.to/#/$user">$display</a>"""
		room.sendMessage(FormattedTextMessageContent(
				"$display: $msg",
				TextMessageFormats.HTML,
				"$displayLink: " + msg.replace("<", "&lt;").replace(">", "&gt;")
		))
	}
	else
		room.sendMessage(TextMessageContent(msg))
}

fun Room.sendMessage(msg : String, user : MatrixId? = null)
		= sendMessage(msg, this, user)

/**
 * Sends a HTML message in a room, optionally adding a `user: ` in front of the message. For this to be possible the
 * message must not be enclosed in a `<p>` tag!
 */
fun sendHtmlMessage(plain : String, html : String, room : Room, user : MatrixId? = null)
{
	@Suppress("NAME_SHADOWING") var plain = plain
	@Suppress("NAME_SHADOWING") var html = html
	
	if (user != null)
	{
		val display : String = adminClient.displayname(user) ?: user.toString()
		val displayLink = """<a href="https://matrix.to/#/$user">$display</a>"""
		plain = "$display: $plain"
		html = "$displayLink: $html"
	}
	
	room.sendMessage(FormattedTextMessageContent(plain, TextMessageFormats.HTML, html))
}

fun Room.sendHtmlMessage(plain : String, html : String, user : MatrixId? = null)
		= sendHtmlMessage(plain, html, this, user)

/**
 * Sends a greeting message to that room.
 */
fun greetRoom(room : Room)
{
	logger.info("Greeting $room")
	room.sendHtmlMessage(
			"""Greetings everybody. I'm the "administrator" of the signal bridge to matrix. Everybody who'd like to """ +
					"""use the bridge, send me the command 'register'.""",
			"""<p>Greetings everybody. I'm the "administrator" of the <a href="${MextrixMatrixASSignal.URL}">signal """ +
					"""bridge to matrix</a>. Everybody who'd like to use the bridge, send me the command <code>register</code>."""
	)
}

/**
 * Handles a message received in a room.
 */
fun handleRoomMessage(roomId : RoomId, senderId : MatrixId, msg : MessageContent)
{
	if (msg.msgtype != MessageTypes.TEXT)
	{
		logger.warn("Received message of type ${msg.msgtype}, ignoring")
		return
	}
	val body = msg.body
	
	val (room, sender, bridgeUser) = transaction {
		val room = AdminRoom.find { AdminRooms.roomId eq "$roomId" }.firstOrNull()
		val sender = AdminRoomMember.find { AdminRoomMembers.userId eq "$senderId" }.firstOrNull()
		val bridgeUser = BridgeUser.find { BridgeUsers.userId eq "$senderId" }.firstOrNull()
		Triple(room, sender, bridgeUser)
	}
	
	if (sender == null)
	{
		logger.warn("Received message from $senderId but it is not registered yet")
		return
	}
	if (room == null)
	{
		logger.warn("Received message in room $roomId but it is not registered yet")
		return
	}
	
	val cmd = parseCommand(body)
	val matrixRoom = Room(adminClient, roomId)
	
	when (cmd)
	{
		Command.REGISTER   ->
		{
			if (sender.status == AdminRoomMemberStatus.UNREGISTERED)
			{
				transaction {
					sender.status = AdminRoomMemberStatus.ISSUED_REGISTRATION
				}
				
				val registration = newSignalRegistration(matrixRoom, sender)
				matrixRoom.sendHtmlMessage(
						"Thanks! Please scan the following QR code using your phone (Go to Settings -> Linked Devices):",
						"Thanks! Please scan the following QR code using your phone (Go to <i>Settings</i> -> <i>Linked Devices</i>):",
						senderId)
				val imageMessage = ImageMessageContent("Signal Registration QR Code")
				imageMessage.uploadImage(registration, adminClient)
				matrixRoom.sendMessage(imageMessage)
			}
			else
				matrixRoom.sendHtmlMessage(
						"You have already initiated a link. Please remove your previous request before issuing a new one. (See \"nuke\")",
						"You have already initiated a link. Please remove your previous request before issuing a new one. (See <code>nuke</code>)",
						senderId
				)
		}
		Command.SYNC       ->
		{
			if (bridgeUser == null)
				matrixRoom.sendMessage("You need to register before you can sync", senderId)
			else
			{
				SignalConnections.sendMessage(bridgeUser.number,
						Request.newBuilder().setType(Request.Type.CONTACTS).build())
				SignalConnections.sendMessage(bridgeUser.number,
						Request.newBuilder().setType(Request.Type.GROUPS).build())
				matrixRoom.sendMessage("Sync is going on, please wait", senderId)
			}
		}
		Command.NUKE       ->
		{
			if (sender.status != AdminRoomMemberStatus.UNREGISTERED)
			{
				transaction { sender.status = AdminRoomMemberStatus.UNREGISTERED }
				if (bridgeUser == null)
					matrixRoom.sendMessage("You need to register before you can nuke", senderId)
				else
				{
					transaction {
						AsRoom.find { AsRooms.bridgeUser eq bridgeUser.id }.forEach {
							it.matrixRoom.deleteCompletely()
						}
						bridgeUser.delete()
					}
				}
			}
		}
		Command.STATUS     ->
		{
			when (sender.status)
			{
				AdminRoomMemberStatus.REGISTERED          ->
				{
					val number = bridgeUser!!.number
					matrixRoom.sendMessage("You are currently registered with this bridge, your number is $number")
				}
				AdminRoomMemberStatus.ISSUED_REGISTRATION ->
				{
					matrixRoom.sendMessage("Registration is currently in process, "
							+ "please scan your QR code if you haven't done so already")
				}
				AdminRoomMemberStatus.UNREGISTERED        ->
				{
					matrixRoom.sendMessage("Type \"register\" to link your Signal account with this bridge")
				}
			}
		}
		Command.HELP       ->
			matrixRoom.sendHtmlMessage(
					"I know about the following commands:\n\n" +
							Command.values().filter { it.description != null }.joinToString("\n") {
								" - ${it.name.toLowerCase()}: ${it.description}"
							} + "\n\n${MextrixMatrixASSignal.NAME} ${MextrixMatrixASSignal.VERSION} (${MextrixMatrixASSignal.URL})",
					"""<p>I know about the following commands:</p><ul><li>""" +
							Command.values().filter { it.description != null }.joinToString("</li><li>") {
								"<strong><code>${it.name.toLowerCase()}</code></strong>: ${it.description}"
							} + """</li></ul><p><a href="${MextrixMatrixASSignal.URL}">${MextrixMatrixASSignal.NAME} ${MextrixMatrixASSignal.VERSION}</a></p>"""
			)
		
		Command.SIGNALINFO ->
		{
			if (bridgeUser == null)
				matrixRoom.sendMessage("You have to be registered to use this command")
			else
			{
				val arguments = splitArguments(0, body)
				if (arguments.isEmpty())
					matrixRoom.sendMessage("You have not provided a phone number to check")
				else
				{
					val no = bridgeUser.number
					val number = phoneRegex.find(arguments[0])?.value
					if (number != null)
					{
						try
						{
							val address = SignalServiceAddress(number)
							val profile = SignalConnections.retrieveProfile(no, address)
							matrixRoom.sendMessage("The profile's identity key is ${profile.identityKey}")
						}
						catch (notFound : NonSuccessfulResponseCodeException)
						{
							matrixRoom.sendMessage("The user $number is not registered with Signal")
						}
					}
					else
						matrixRoom.sendMessage("Your phone number has not the correct format, please use E.164")
				}
			}
		}
		Command.STARTCHAT  -> TODO()
		Command.UNKNOWN    ->
		{
			matrixRoom.sendMessage("Unknown command! Type \"help\" for available commands")
		}
	}
}
