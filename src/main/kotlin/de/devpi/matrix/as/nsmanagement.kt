/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix.`as`

import de.devpi.matrix.*
import de.devpi.matrix.Configuration.adminUserConfig
import de.devpi.matrix.Configuration.appserviceConfig
import de.devpi.matrix.Configuration.hsConfig
import de.devpi.matrix.db.*
import msrd0.matrix.client.*
import msrd0.matrix.client.event.MatrixEventTypes.*
import msrd0.matrix.client.event.state.RoomPowerLevels
import msrd0.matrix.client.event.state.RoomPowerLevels.Companion.ADMINISTRATOR
import msrd0.matrix.client.event.state.RoomPowerLevels.Companion.OWNER
import msrd0.matrix.client.event.state.RoomPowerLevels.Companion.USER
import org.apache.commons.codec.binary.Base64.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory
import java.net.URI
import java.nio.charset.StandardCharsets.*
import java.security.MessageDigest
import de.devpi.matrix.`as`.logger as asLogger

private val logger = LoggerFactory.getLogger(currClassName())

private val md5 = MessageDigest.getInstance("MD5")

/**
 * Generates a user id for the [bridgeUser] and the contact [userNumber].
 */
fun computeUserId(bridgeUser : String, userNumber : String) : MatrixId
{
	val hash = encodeBase64URLSafeString(md5.digest("$bridgeUser$userNumber".toByteArray(UTF_8)))
	return MatrixId("${appserviceConfig.signalNamespace}vu$hash", hsConfig.domain)
}

fun getUser(mxId : MatrixId) : AsUser
{
	return transaction {
		AsUser.find { AsUsers.userId eq "$mxId" }.getOrNew {
			asLogger.info("Creating user $mxId")
			val client = MatrixClient.registerFromAs(mxId.localpart,
					HomeServer(hsConfig.domain, URI(hsConfig.baseUri)),
					token = adminUserConfig.token)
			if (client.deviceId == null)
				asLogger.warn("Newly created client's device id is null")
			else
				client.updateDeviceDisplayName(client.deviceId!!,
						"MatrixSignalBridge (${MextrixMatrixASSignal.URL})")
			this.deviceId = client.deviceId!!
			this.userId = mxId
		}
	}
}

fun MatrixClient.getRoom(id : RoomId) : Room
		= Room(this, id)

/**
 * Creates a new room. If [alias] is not null, this alias is added for the room and set as the canonical alias.
 */
fun MatrixClient.createRoomWithPowerLevels(alias : RoomAlias? = null) : Room
{
	val room = this.createRoom(public = false)
	
	room.powerLevels = RoomPowerLevels(
			// all user stuff is admin-only
			invite = ADMINISTRATOR,
			ban = ADMINISTRATOR,
			kick = ADMINISTRATOR,
			// moderation stuff is admin-only
			redact = ADMINISTRATOR,
			stateDefault = ADMINISTRATOR,
			// events by every low-level user
			eventsDefault = USER,
			usersDefault = USER,
			// evil events admin-only
			events = mutableMapOf(
					// appearance
					ROOM_AVATAR to ADMINISTRATOR,
					ROOM_NAME to ADMINISTRATOR,
					ROOM_TOPIC to ADMINISTRATOR,
					// settings
					ROOM_CANONICAL_ALIAS to ADMINISTRATOR,
					ROOM_ENCRYPTION to ADMINISTRATOR,
					ROOM_HISTORY_VISIBILITY to ADMINISTRATOR,
					ROOM_POWER_LEVELS to ADMINISTRATOR
			),
			// and we are the owner of course
			users = mutableMapOf(
					this.id to OWNER
			)
	)
	
	if (alias != null)
	{
		room.addAlias(alias)
		room.canonicalAlias = alias
		asLogger.info("Created room ${room.id} with alias $alias")
	}
	else
		asLogger.info("Created room ${room.id} without an alias")
	
	return room
}

/**
 * Completely delete a room by making all users leave the room. Please make sure that the client assigned to this room
 * has the power to kick other users!!
 */
fun Room.deleteCompletely()
{
	for (member in members)
	{
		if (member == client.id)
			continue
		
		val asUser = transaction { AsUser.find { AsUsers.userId eq "$member" }.firstOrNull() }
		if (asUser != null)
			Room(asUser.matrixClient, id).forget()
		else
			kick(member)
	}
	forget()
}