/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix.`as`

import de.devpi.matrix.Configuration.adminUserConfig
import de.devpi.matrix.Configuration.appserviceConfig
import de.devpi.matrix.Configuration.hsConfig
import msrd0.matrix.client.*
import msrd0.matrix.client.event.MessageContent
import org.slf4j.*
import java.net.URI

val hs = HomeServer(hsConfig.domain, URI(hsConfig.baseUri))

object SignalAdminClient
{
	val logger : Logger = LoggerFactory.getLogger(SignalAdminClient::class.java)
	
	val admin = getUser(MatrixId(adminUserConfig.localpart, hs.domain))
	val adminClient = admin.matrixClient
	
	init
	{
		adminClient.updateDisplayname("Signal-Bridge - Admin")
	}
	
	fun onRoomInvitation(room : RoomInvitation) : Boolean
	{
		logger.info("Accepting new Room Invitation: $room")
		room.accept()
		return true
	}
	
	fun onRoomJoin(room : Room) : Boolean
	{
		logger.info("Joined Room $room")
		registerRoom(room)
		return true
	}
	
	fun onRoomMessage(room : RoomId, sender : MatrixId, msg : MessageContent) : Boolean
	{
		if (sender != adminClient.id)
		{
			logger.info("Received Message in Room $room from $sender: ${msg.body}")
			handleRoomMessage(room, sender, msg)
		}
		return true
	}
}

object SignalBotClient
{
	val logger : Logger = LoggerFactory.getLogger(SignalBotClient::class.java)
	
	val bot = getUser(MatrixId("${appserviceConfig.signalNamespace}bot", hs.domain))
	val botClient = bot.matrixClient
	
	init
	{
		botClient.updateDisplayname("Signal-Bridge - Bot")
	}
}
