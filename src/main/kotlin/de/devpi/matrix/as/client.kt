/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix.`as`

import de.devpi.matrix.db.*
import de.devpi.matrix.sha1
import msrd0.matrix.client.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.net.URI

class AsClient(hs : HomeServer, id : MatrixId) : MatrixClient(hs, id)
{
	companion object
	{
		val txnIdLock = Object()
	}
	
	@JvmOverloads
	constructor(domain : String, localpart : String, hsDomain : String = domain, hsBaseUri : URI = URI("https://$hsDomain/"))
			: this(HomeServer(hsDomain, hsBaseUri), MatrixId(localpart, domain))
	
	override val nextTxnId : Long
		get()
		{
			return synchronized(txnIdLock) {
				transaction {
					val dao = LastUsedTxnId.find { LastUsedTxnIds.tokenSha1 eq sha1(token ?: throw NoTokenException()) }.firstOrNull()
					if (dao != null)
					{
						dao.lastTxnId++
						return@transaction dao.lastTxnId
					}
					else
					{
						LastUsedTxnId.new {
							token = this@AsClient.token ?: throw NoTokenException()
							lastTxnId = 0
						}
						return@transaction 0L
					}
				}
			}
		}
}
