/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix

import org.slf4j.*
import java.lang.reflect.*
import java.security.*

object DirtyHacks
{
	
	private val logger : Logger = LoggerFactory.getLogger(DirtyHacks::class.java)
	
	/**
	 * Copied from [StackOverflow](https://stackoverflow.com/questions/1179672/how-to-avoid-installing-unlimited-strength-jce-policy-files-when-deploying-an)
	 */
	@JvmStatic
	public fun removeCryptographyRestrictions()
	{
		if (!isRestrictedCryptography())
		{
			logger.info("Cryptography restrictions removal not needed")
			return
		}
		try
		{
			/*
         	 * Do the following, but with reflection to bypass access checks:
        	 *
        	 * JceSecurity.isRestricted = false;
        	 * JceSecurity.defaultPolicy.perms.clear();
        	 * JceSecurity.defaultPolicy.add(CryptoAllPermission.INSTANCE);
        	 */
			val jceSecurity = Class.forName("javax.crypto.JceSecurity")
			val cryptoPermissions = Class.forName("javax.crypto.CryptoPermissions")
			val cryptoAllPermission = Class.forName("javax.crypto.CryptoAllPermission")
			
			val isRestrictedField = jceSecurity.getDeclaredField("isRestricted")
			isRestrictedField.isAccessible = true
			val modifiersField = Field::class.java.getDeclaredField("modifiers")
			modifiersField.isAccessible = true
			modifiersField.setInt(isRestrictedField, isRestrictedField.modifiers and Modifier.FINAL.inv())
			isRestrictedField.set(null, false)
			
			val defaultPolicyField = jceSecurity.getDeclaredField("defaultPolicy")
			defaultPolicyField.isAccessible = true
			val defaultPolicy = defaultPolicyField.get(null) as PermissionCollection
			
			val perms = cryptoPermissions.getDeclaredField("perms")
			perms.isAccessible = true
			(perms.get(defaultPolicy) as MutableMap<*, *>).clear()
			
			val instance = cryptoAllPermission.getDeclaredField("INSTANCE")
			instance.isAccessible = true
			defaultPolicy.add(instance.get(null) as Permission)
			
			logger.info("Successfully removed cryptography restrictions")
		}
		catch (e : Exception)
		{
			logger.warn("Failed to remove cryptography restrictions", e)
		}
		
	}
	
	@JvmStatic
	private fun isRestrictedCryptography() : Boolean
	{
		// This matches Oracle Java 7 and 8, but not Java 9 or OpenJDK.
		val name = System.getProperty("java.runtime.name")
		val ver = System.getProperty("java.version")
		return name != null && name == "Java(TM) SE Runtime Environment"
				&& ver != null && (ver.startsWith("1.7") || ver.startsWith("1.8"))
	}
	
}
