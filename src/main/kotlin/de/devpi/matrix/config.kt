/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import org.slf4j.*
import java.io.File

/**
 * This object holds the configuration of the matrix signal appservice. A configuration file might look like this:
 *
 * ```yaml
 * hs:
 *   domain: matrix.org
 *   baseUri: "https://matrix.org/"
 *   token: aoeiu123
 *
 * admin-user:
 *   localpart: signal-admin
 *   token: aoeiu123
 *
 * db:
 *   host: localhost
 *   port: 5432
 *   name: matrix_appservice_signal
 *   user: postgres
 *   password: thisIsASecret
 * ```
 */
object Configuration
{
	private val logger : Logger = LoggerFactory.getLogger(Configuration::class.java)
	
	class HomeServer
	{
		@JsonProperty
		var domain = ""
		
		@JsonProperty
		var baseUri = ""
		
		@JsonProperty
		var token = ""
	}
	
	class AdminUser
	{
		@JsonProperty
		var localpart = ""
		
		@JsonProperty
		var token = ""
	}
	
	class DB
	{
		@JsonProperty
		var type = "sqlite3"
		
		@JsonProperty
		var file = "default.db"
		
		@JsonProperty
		var host = "localhost"
		
		@JsonProperty
		var port = 5432
		
		@JsonProperty
		var name = "matrix_appservice_signal"
		
		@JsonProperty
		var user = "postgres"
		
		@JsonProperty
		var password = ""
	}
	
	class AppService
	{
		@JsonProperty("signal-ns")
		var signalNamespace = "signal_"
	}
	
	private class Root
	{
		@JsonProperty
		var hs = HomeServer()
		
		@JsonProperty("admin-user")
		var adminUser = AdminUser()
		
		@JsonProperty
		var db = DB()
		
		@JsonProperty
		var appservice = AppService()
	}
	
	private var root = Root()
	
	val hsConfig get() = root.hs
	
	val adminUserConfig get() = root.adminUser
	
	val dbConfig get() = root.db
	
	val appserviceConfig get() = root.appservice
	
	/**
	 * Load the yaml configuration from `file`.
	 */
	@JvmStatic
	fun loadConfig(file : File)
	{
		logger.info("Loading configuration from $file")
		val factory = YAMLFactory()
		val mapper = ObjectMapper(factory)
		root = mapper.readValue(file, Root::class.java)
	}
	
	/**
	 * Load the yaml configuration from `/etc/matrix-appservice-signal.yml`.
	 */
	@JvmOverloads
	@JvmStatic
	fun loadConfig(path : String = "")
	{
		if (path == "")
		{
			val f = File("/etc/matrix-appservice-signal.yml")
			if (f.exists())
				loadConfig(f)
		}
		else
			loadConfig(File(path))
	}
}
