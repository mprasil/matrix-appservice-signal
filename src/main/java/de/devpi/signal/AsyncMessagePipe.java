/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.signal;

import static de.devpi.matrix.as.SignalactionsKt.*;

import java.io.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Consumer;

import okhttp3.*;
import okio.ByteString;
import org.slf4j.*;
import org.whispersystems.libsignal.InvalidVersionException;
import org.whispersystems.signalservice.api.messages.SignalServiceEnvelope;
import org.whispersystems.signalservice.api.util.CredentialsProvider;
import org.whispersystems.signalservice.internal.websocket.*;

public class AsyncMessagePipe extends WebSocketConnection
{
	private static Logger log = LoggerFactory.getLogger(AsyncMessagePipe.class);
	
	WebSocket client = null;
	Set<Consumer<SignalServiceEnvelope>> listeners = new HashSet<>();
	CredentialsProvider credentialsProvider;
	
	public AsyncMessagePipe(CredentialsProvider credentialsProvider)
	{
		super(signalUrls[0].getUrl(), signalUrls[0].getTrustStore(), credentialsProvider, "OWD");
		this.credentialsProvider = credentialsProvider;
	}
	
	@Override
	public void onOpen(WebSocket webSocket, Response response)
	{
		try
		{
			client = webSocket;
			Class<WebSocketConnection> clazz = WebSocketConnection.class;
			Field connectedField = clazz.getDeclaredField("connected");
			connectedField.setAccessible(true);
			connectedField.setBoolean(this, true);
			Field attemptsField = clazz.getDeclaredField("attempts");
			attemptsField.setAccessible(true);
			attemptsField.setInt(this, 0);
		}
		catch (NoSuchFieldException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public void onMessage(WebSocket socket, ByteString payload)
	{
		super.onMessage(socket, payload);
		try
		{
			WebSocketProtos.WebSocketMessage message = WebSocketProtos.WebSocketMessage.parseFrom(payload.toByteArray());
			
			if (message.getType().getNumber() == WebSocketProtos.WebSocketMessage.Type.REQUEST_VALUE)
			{
				WebSocketProtos.WebSocketRequestMessage request = message.getRequest();
				log.info("onMessage: New request message; verb={}, path={}", request.getVerb(), request.getPath());
				WebSocketProtos.WebSocketResponseMessage response;
				if (request.getVerb().equals("PUT") && request.getPath().equals("/api/v1/message"))
				{
					response = WebSocketProtos.WebSocketResponseMessage.newBuilder().setId(request.getId())
							.setStatus(200)
							.setMessage("OK")
							.build();
					super.sendResponse(response);
					try
					{
						SignalServiceEnvelope envelope = new SignalServiceEnvelope(request.getBody().toByteArray(),
								credentialsProvider.getSignalingKey());
						for (Consumer<SignalServiceEnvelope> listener : listeners)
						{
							listener.accept(envelope);
						}
					}
					catch (InvalidVersionException | IOException e)
					{
						e.printStackTrace();
					}
				}
				else
				{
					response = WebSocketProtos.WebSocketResponseMessage.newBuilder().setId(request.getId())
							.setStatus(400)
							.setMessage("UNREGISTERED")
							.build();
					super.sendResponse(response);
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void addListener(Consumer<SignalServiceEnvelope> listener)
	{
		listeners.add(listener);
	}
	
	public void removeListener(int hashCode)
	{
		listeners.removeIf((Consumer<SignalServiceEnvelope> element) -> element.hashCode() == hashCode);
	}
	
	public void sendKeepAlive()
	{
		byte[] message = WebSocketProtos.WebSocketMessage.newBuilder()
				.setType(WebSocketProtos.WebSocketMessage.Type.REQUEST)
				.setRequest(WebSocketProtos.WebSocketRequestMessage.newBuilder()
						.setId(System.currentTimeMillis())
						.setPath("/v1/keepalive")
						.setVerb("GET")
						.build()).build()
				.toByteArray();
		
		client.send(ByteString.of(message));
	}
}
