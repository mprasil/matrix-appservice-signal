/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.signal;

import java.awt.image.RenderedImage;
import java.io.*;
import java.net.URLEncoder;
import java.security.*;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

import com.google.zxing.*;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.*;
import org.whispersystems.libsignal.*;
import org.whispersystems.libsignal.InvalidKeyException;
import org.whispersystems.libsignal.ecc.*;
import org.whispersystems.libsignal.util.KeyHelper;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.util.CredentialsProvider;
import org.whispersystems.signalservice.internal.push.*;
import org.whispersystems.signalservice.internal.util.Base64;

/**
 * Handles registration of Signal clients by linking to an existing account.
 * <p>
 * To get started create a new instance of this class with server information and your custom user
 * agent (typically the name of your application). Create a new QR code and wait for the scan to be finished. After linking
 * you can create a new SignalServiceAccountManager and start using the Signal library. Make sure your store the AccountInfo
 * carefully, otherwise you have to link your accounts again.
 * <p>
 * As soon as you have finished registration you should generate some keys and push them to the
 * server. From here on everything should be pretty straightforward and not different from
 * sending/receiving messages on your android device.
 *
 * @author Julius Lehmann
 */
public class SignalRegistration
{
	
	private final static Logger log = LoggerFactory.getLogger(SignalRegistration.class);
	private final ProvisioningSocket provisioningSocket;
	private final SignalServiceUrl[] urls;
	private final DynamicCredentialsProvider credentialsProvider;
	private SecureRandom random;
	private IdentityKeyPair keyPair;
	private Consumer<AccountInfo> consumer;
	private int regID;
	private String signalingKey;
	private boolean scanned = false;
	
	public SignalRegistration(SignalServiceUrl urls[])
	{
		this(urls, "OWD");
	}
	
	protected SignalRegistration(SignalServiceUrl[] urls, String userAgent)
	{
		Security.addProvider(new BouncyCastleProvider());
		this.provisioningSocket = new ProvisioningSocket(urls, userAgent);
		random = null;
		try
		{
			random = SecureRandom.getInstance("SHA1PRNG");
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		String password = getRString(16);
		password = password.substring(0, password.length() - 2);
		credentialsProvider = new DynamicCredentialsProvider("", password, null,
				SignalServiceAddress.DEFAULT_DEVICE_ID);
		this.urls = urls;
		
		keyPair = KeyHelper.generateIdentityKeyPair();
		regID = KeyHelper.generateRegistrationId(false);
		signalingKey = getRString(52);
	}
	
	public String getNewDeviceUuid()
			throws IOException, TimeoutException
	{
		return provisioningSocket.getProvisioningUuid().getUuid();
	}
	
	public RenderedImage getAuthenticationQRCode()
	{
		try
		{
			QRCodeWriter writer = new QRCodeWriter();
			String uuid = getNewDeviceUuid();
			String publicKey = Base64.encodeBytesWithoutPadding(keyPair.getPublicKey().serialize());
			String url = String.format("tsdevice:/?uuid=%s&pub_key=%s", URLEncoder.encode(uuid, "UTF-8"),
					URLEncoder.encode(publicKey, "UTF-8"));
			BitMatrix bm = writer.encode(url, BarcodeFormat.QR_CODE, 1024, 1024);
			return MatrixToImageWriter.toBufferedImage(bm);
		}
		catch (IOException | TimeoutException | WriterException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public void onScan(Consumer<AccountInfo> callback)
			throws IOException, TimeoutException
	{
		consumer = callback;
		submitLinkRegistration(keyPair, signalingKey, false, true, regID, "MatrixSignalBridge");
	}
	
	public synchronized void submitLinkRegistration(IdentityKeyPair temporaryID, String signalKey,
													boolean smsSupport, boolean messageFetching, int registrationId, String deviceName)
			throws IOException, TimeoutException
	{

    /* access the ProvisionMessage which is transmitted through the open websocket after scanning the
	 * QR code with your device
     */
		provisioningSocket.getProvisioningMessage(temporaryID, (ProvisioningProtos.ProvisionMessage msg) -> {
			if (!scanned)
			{
				//retrieve all information and generate local identity
				String provisionCode = msg.getProvisioningCode();
				ECPublicKey pubKey = null;
				try
				{
					pubKey = Curve.decodePoint(msg.getIdentityKeyPublic().toByteArray(), 0);
				}
				catch (InvalidKeyException e)
				{
					e.printStackTrace();
				}
				credentialsProvider.setUser(msg.getNumber());
				ECPrivateKey privKey = Curve.decodePrivatePoint(msg.getIdentityKeyPrivate().toByteArray());
				IdentityKeyPair identity = new IdentityKeyPair(new IdentityKey(pubKey), privKey);
				credentialsProvider.setSignalingKey(signalingKey);
				//register local device
				int deviceID;
				try
				{
					PushServiceSocket socket = new PushServiceSocket(urls, credentialsProvider, "OWD");
					deviceID = socket.submitDeviceLinkRegistration(provisionCode, signalKey, smsSupport, messageFetching,
							registrationId, deviceName);
					credentialsProvider.setUser(msg.getNumber() + "." + deviceID);
					consumer.accept(new AccountInfo(identity, registrationId, credentialsProvider));
					scanned = true;
				}
				catch (ReflectiveOperationException e)
				{
					log.error("Reflective exception", e);
				}
				catch (IOException e)
				{
					log.error("Exception while sending", e);
				}
			}
		});
	}
	
	public String getRString(int size)
	{
		byte[] bytes = new byte[size];
		random.nextBytes(bytes);
		return Base64.encodeBytes(bytes);
	}
	
	/**
	 * Helper class
	 */
	public class AccountInfo
	{
		
		private final IdentityKeyPair identity;
		private final CredentialsProvider provider;
		private final int registrationId;
		
		AccountInfo(IdentityKeyPair identity, int registrationId, CredentialsProvider provider)
		{
			this.identity = identity;
			this.provider = provider;
			this.registrationId = registrationId;
		}
		
		/**
		 * @return The account's permanent IdentityKeyPair
		 */
		public IdentityKeyPair getIdentity()
		{
			return identity;
		}
		
		public CredentialsProvider getCredentialsProvider()
		{
			return provider;
		}
		
		public int getRegistrationId()
		{
			return registrationId;
		}
	}
	
}
