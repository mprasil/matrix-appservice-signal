/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.signal;

import java.io.*;
import java.security.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Consumer;
import javax.net.ssl.*;

import de.devpi.signal.ProvisioningProto.ProvisioningUuid;

import com.google.protobuf.InvalidProtocolBufferException;
import okhttp3.*;
import org.slf4j.*;
import org.whispersystems.libsignal.*;
import org.whispersystems.libsignal.InvalidKeyException;
import org.whispersystems.libsignal.util.guava.Optional;
import org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessage;
import org.whispersystems.signalservice.internal.push.SignalServiceUrl;
import org.whispersystems.signalservice.internal.util.*;
import org.whispersystems.signalservice.internal.websocket.WebSocketProtos;
import org.whispersystems.signalservice.internal.websocket.WebSocketProtos.*;

public class ProvisioningSocket extends WebSocketListener
{
	
	private static final String PROVISIONING_SOCKET_PATH = "/v1/websocket/provisioning/?agent=%s";
	private static final String KEEPALIVE_PATH = "/v1/keepalive/provisioning";
	private static final String ADDRESS_PATH = "/v1/address";
	private static final String MESSAGE_PATH = "/v1/message";
	private static final long timeoutMillis = 10000;
	
	private final static Logger log = LoggerFactory.getLogger(ProvisioningSocket.class);
	
	private final SecureRandom random;
	private final SignalConnectionInformation[] signalConnectionInformation;
	private String userAgent;
	private WebSocket socket;
	private KeepAliveHelper keepAliveHelper;
	private ScheduledExecutorService exec;
	private boolean connected = false;
	private ProvisioningUuid requestedUuid;
	private ProvisionMessage provisionMessage;
	private IdentityKeyPair tmpId;
	
	private Consumer<ProvisionMessage> consumer;
	
	public ProvisioningSocket(SignalServiceUrl[] urls, String userAgent)
	{
		try
		{
			this.random = SecureRandom.getInstance("SHA1PRNG");
		}
		catch (NoSuchAlgorithmException e)
		{
			throw new AssertionError(e);
		}
		this.signalConnectionInformation = new SignalConnectionInformation[urls.length];
		this.userAgent = userAgent;
		
		for (int i = 0; i < urls.length; i++)
		{
			signalConnectionInformation[i] = new SignalConnectionInformation(urls[i]);
		}
		
		keepAliveHelper = new KeepAliveHelper();
	}
	
	public synchronized ProvisioningUuid getProvisioningUuid()
			throws IOException, TimeoutException
	{
		if (!connected)
		{
			connect();
			//websocket connect
			connected = true;
		}
		long startTime = System.currentTimeMillis();
		while (socket != null && requestedUuid == null
				&& startTime - System.currentTimeMillis() < timeoutMillis)
		{
			Util.wait(this, Math.max(1, timeoutMillis - (startTime - System.currentTimeMillis())));
		}
		
		if (requestedUuid == null && socket == null)
		{
			throw new IOException("Connection closed!");
		}
		else if (requestedUuid == null)
		{
			throw new TimeoutException("Timeout exceeded");
		}
		else
		{
			return requestedUuid;
		}
	}
	
	public synchronized void getProvisioningMessage(IdentityKeyPair tempIdentity, Consumer<ProvisionMessage> callback)
			throws TimeoutException, IOException
	{
		tmpId = tempIdentity;
		this.consumer = callback;
	}
	
	private synchronized void connect()
	{
		try
		{
			SignalConnectionInformation conInfo = getRandom();
			
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, conInfo.getTrustManagers(), null);
			
			OkHttpClient client = new OkHttpClient.Builder()
					.sslSocketFactory(context.getSocketFactory(),
							(X509TrustManager) conInfo.getTrustManagers()[0])
					.connectTimeout(timeoutMillis, TimeUnit.MILLISECONDS)
					.readTimeout(timeoutMillis, TimeUnit.MILLISECONDS).build();
			
			String url = conInfo.getUrl().replace("http", "ws")
					.concat(String.format(PROVISIONING_SOCKET_PATH, userAgent));
			Request.Builder requestBuilder = new Request.Builder().url(url);
			
			if (userAgent != null)
			{
				requestBuilder.addHeader("X-Signal-Agent", userAgent);
			}
			
			this.connected = false;
			socket = client.newWebSocket(requestBuilder.build(), this);
			
		}
		catch (NoSuchAlgorithmException | KeyManagementException e)
		{
			e.printStackTrace();
			log.error("Error initializing SSL");
		}
	}
	
	private synchronized void disconnect()
	{
		log.info("Disconnecting ...");
		if (socket != null && connected)
		{
			if (exec != null)
			{
				exec.shutdown();
				exec = null;
			}
			socket.close(1000, "OK");
		}
	}
	
	
	@Override
	public void onClosed(WebSocket webSocket, int code, String reason)
	{
		connected = false;
		socket = null;
		if (exec != null)
		{
			exec.shutdown();
			exec = null;
		}
		log.info("Websocket closed: " + code + " " + reason);
	}
	
	@Override
	public void onFailure(WebSocket webSocket, Throwable t, Response response)
	{
		log.warn("WebSocket failure ...");
		t.printStackTrace();
		if (socket != null)
		{
			onClosed(socket, 1000, "OK");
		}
	}
	
	@Override
	public synchronized void onOpen(WebSocket webSocket, Response response)
	{
		log.info("Server responded: " + response.code());
		if (socket != null && exec == null)
		{
			exec = Executors.newSingleThreadScheduledExecutor();
			exec.scheduleAtFixedRate(keepAliveHelper, 0, 55, TimeUnit.SECONDS);
			connected = true;
		}
	}
	
	@Override
	public synchronized void onMessage(WebSocket webSocket, okio.ByteString bytes)
	{
		log.info("onMessage() ...");
		try
		{
			WebSocketProtos.WebSocketMessage message = WebSocketProtos.WebSocketMessage
					.parseFrom(bytes.toByteArray());
			if (message.getType() == WebSocketProtos.WebSocketMessage.Type.RESPONSE)
			{
				log.info("Incoming response message, doing nothing");
				//System.out.println(message.getResponse().toString());
			}
			else if (message.getType() == WebSocketProtos.WebSocketMessage.Type.REQUEST)
			{
				log.info("Incoming request message");
				WebSocketRequestMessage requestMessage = message.getRequest();
				if (Objects.equals(requestMessage.getVerb(), "PUT") && Objects
						.equals(requestMessage.getPath(), ADDRESS_PATH))
				{
					requestedUuid = ProvisioningUuid.parseFrom(requestMessage.getBody());
					respondOK(webSocket, requestMessage);
				}
				else if (Objects.equals(requestMessage.getVerb(), "PUT") && Objects
						.equals(requestMessage.getPath(), MESSAGE_PATH))
				{
					provisionMessage = ProvisionMessage.parseFrom(requestMessage.getBody());
					respondOK(webSocket, requestMessage);
				}
				else
				{
					log.warn("Unknown websocket message: " + requestMessage.getPath());
				}
			}
		}
		catch (InvalidProtocolBufferException e)
		{
			log.warn("Exception while trying to parse message, Aborting ...");
			e.printStackTrace();
		}
		if (provisionMessage != null)
		{
			disconnect();
			ProvisionMessage msg;
			try
			{
				msg = ProvisioningCipher.decrypt(tmpId, provisionMessage.toByteArray());
				consumer.accept(msg);
			}
			catch (InvalidKeyException e)
			{
				throw new AssertionError(e);
			}
			catch (InvalidProtocolBufferException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void onClosing(WebSocket webSocket, int code, String reason)
	{
		log.info("onClosing() ...");
		webSocket.close(1000, "OK");
	}
	
	
	private void respondOK(WebSocket webSocket, WebSocketRequestMessage message)
	{
		WebSocketResponseMessage.Builder builder = WebSocketProtos.WebSocketResponseMessage
				.newBuilder().setMessage("OK").setId(message.getId()).setStatus(200);
		WebSocketProtos.WebSocketMessage.Builder msgBuilder = WebSocketProtos.WebSocketMessage
				.newBuilder().setType(WebSocketProtos.WebSocketMessage.Type.RESPONSE)
				.setResponse(builder);
		WebSocketProtos.WebSocketMessage msg = msgBuilder.build();
		okio.ByteString bS = okio.ByteString.of(msg.toByteArray());
		webSocket.send(bS);
	}
	
	private SignalConnectionInformation getRandom()
	{
		return signalConnectionInformation[random.nextInt(signalConnectionInformation.length)];
	}
	
	private void sendKeepAlive()
			throws IOException
	{
		if (keepAliveHelper != null && socket != null)
		{
			byte[] message = WebSocketProtos.WebSocketMessage.newBuilder()
					.setType(WebSocketProtos.WebSocketMessage.Type.REQUEST)
					.setRequest(WebSocketRequestMessage.newBuilder()
							.setId(System.currentTimeMillis())
							.setPath(KEEPALIVE_PATH)
							.setVerb("GET")
							.build()).build()
					.toByteArray();
			
			if (!socket.send(okio.ByteString.of(message)))
			{
				throw new IOException("Write failed!");
			}
		}
	}
	
	public static class SignalConnectionInformation
	{
		
		private final String url;
		private final Optional<String> hostHeader;
		private final Optional<ConnectionSpec> connectionSpec;
		private final TrustManager[] trustManagers;
		
		public SignalConnectionInformation(SignalServiceUrl signalServiceUrl)
		{
			this.url = signalServiceUrl.getUrl();
			this.hostHeader = signalServiceUrl.getHostHeader();
			this.connectionSpec = signalServiceUrl.getConnectionSpec();
			this.trustManagers = BlacklistingTrustManager.createFor(signalServiceUrl.getTrustStore());
		}
		
		public String getUrl()
		{
			return url;
		}
		
		public Optional<String> getHostHeader()
		{
			return hostHeader;
		}
		
		public TrustManager[] getTrustManagers()
		{
			return trustManagers;
		}
		
		public Optional<ConnectionSpec> getConnectionSpec()
		{
			return connectionSpec;
		}
	}
	
	private class KeepAliveHelper implements Runnable
	{
		
		@Override
		public void run()
		{
			log.info("Sending Keepalive ...");
			try
			{
				sendKeepAlive();
			}
			catch (IOException e)
			{
				e.printStackTrace();
				log.warn("Exception in Keepalive");
			}
		}
	}
}
