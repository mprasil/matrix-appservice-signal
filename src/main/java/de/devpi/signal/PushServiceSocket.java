/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.signal;

import java.io.*;
import java.lang.reflect.*;

import org.whispersystems.signalservice.api.util.CredentialsProvider;
import org.whispersystems.signalservice.internal.push.*;
import org.whispersystems.signalservice.internal.util.JsonUtil;

/**
 * Created by julius on 08.06.17.
 */
public class PushServiceSocket extends
		org.whispersystems.signalservice.internal.push.PushServiceSocket
{
	
	public PushServiceSocket(SignalServiceUrl[] serviceUrls, CredentialsProvider credentialsProvider,
							 String userAgent)
	{
		super(serviceUrls, credentialsProvider, userAgent);
	}
	
	/**
	 * doing some dirty work here and use methods from superclass
	 *
	 * @param code
	 * @param signalingKey
	 * @param supportsSms
	 * @param fetchesMessages
	 * @param registrationId
	 * @param deviceName
	 * @throws ReflectiveOperationException
	 * @throws IOException
	 */
	int submitDeviceLinkRegistration(String code, String signalingKey, boolean supportsSms,
									 boolean fetchesMessages, int registrationId, String deviceName)
			throws ReflectiveOperationException, IOException
	{
		ConfirmCodeMessage javaJson = new ConfirmCodeMessage(signalingKey, supportsSms, fetchesMessages,
				registrationId, deviceName);
		String json = JsonUtil.toJson(javaJson);
		DeviceId response;
		String DEVICE_PATH;
		try
		{
			Field devPathField = org.whispersystems.signalservice.internal.push.PushServiceSocket.class
					.getDeclaredField("DEVICE_PATH");
			devPathField.setAccessible(true);
			DEVICE_PATH = (String) devPathField.get(null);
			Method makeRequest = org.whispersystems.signalservice.internal.push.PushServiceSocket.class
					.getDeclaredMethod("makeRequest", String.class, String.class, String.class);
			makeRequest.setAccessible(true);
			String responseText = (String) makeRequest
					.invoke(this, String.format(DEVICE_PATH, code), "PUT", json);
			response = JsonUtil.fromJson(responseText, DeviceId.class);
		}
		catch (IllegalAccessException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException e)
		{
			throw new ReflectiveOperationException("Exception while accessing superclass fields", e);
		}
		return response.getDeviceId();
	}
	
	@Override
	public SendMessageResponse sendMessage(OutgoingPushMessageList bundle)
			throws IOException
	{
		super.sendMessage(bundle);
		return new SendMessageResponse(true);
	}
}
