/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix.test

import de.devpi.matrix.*
import de.devpi.matrix.db.*
import msrd0.matrix.client.MatrixId
import org.jetbrains.exposed.dao.*
import org.jetbrains.exposed.sql.SchemaUtils.createMissingTablesAndColumns
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.Assert.*
import org.junit.Test
import org.whispersystems.libsignal.util.KeyHelper
import org.whispersystems.signalservice.internal.util.*
import java.io.File
import javax.sql.rowset.serial.SerialBlob

class DbTest
{
	init
	{
		DirtyHacks.removeCryptographyRestrictions()
		Configuration.loadConfig()
		if (Configuration.dbConfig.type == "sqlite3")
		{
			val file = File.createTempFile("mas", null)
			Configuration.dbConfig.file = file.toString()
			file.deleteOnExit()
		}
		initDb()
		assertNotNull(db.dataSource)
	}
	
	@Test
	fun base64Test()
	{
		val string = "some string"
		val encoded = Base64.encodeBytes(string.toByteArray())
		assertEquals("Base64 encoding not working", string, String(Base64.decode(encoded)))
	}
	
	@Test
	fun signal_db()
	{
		val someOtherPW = "haha"
		val telnumber = "+0123456789"
		val accountStore = transaction {
			val bridgeuser = BridgeUser.new {
				userId = MatrixId("some_id", "somedomain.com")
				this.number = telnumber
			}
			
			return@transaction SignalStorage.new {
				this.bridgeUser = bridgeuser
				this.registrationId = KeyHelper.generateRegistrationId(false)
				this.identityKey = KeyHelper.generateIdentityKeyPair()
				this.credentials = StaticCredentialsProvider("${bridgeUser.number}.50", "bla", getRString(52))
			}
		}
		accountStore.changeEncryption(someOtherPW)
		assertEquals("Database with encryption doesn't work", telnumber,
				accountStore.credentials.user.substringBefore("."))
	}
	
	@Test
	fun getOrNewTest()
	{
		val generated = transaction {
			(null as BridgeUser?).updateOrNew {
				assertTrue(it)
				number = "+012345"
				userId = MatrixId("id_no2", "somedomain.com")
			}
		}
		assertNotNull(generated)
	}
	
	@Test
	fun testBlobs()
	{
		val random = Util.getRandomLengthBytes(20)
		transaction {
			createMissingTablesAndColumns(
					TestTable
			)
			val write = TestDao.new {
				blob = null
			}.id
			val read = TestDao.findById(write)!!
			read.blob = SerialBlob(random)
			val readBlob = read.blob
			assertEquals(String(random), String(readBlob!!.binaryStream.readBytes()))
		}
	}
	
}

object TestTable : LongIdTable(name = "test_table")
{
	val testblob = blob("some_blob").nullable()
}

class TestDao(id : EntityID<Long>) : LongEntity(id)
{
	companion object : LongEntityClass<TestDao>(TestTable)
	
	var blob by TestTable.testblob
}
