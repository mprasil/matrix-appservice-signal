/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix.test

import de.devpi.matrix.DirtyHacks
import de.devpi.matrix.db.secureRandom
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.junit.Assert.*
import org.junit.Test
import org.whispersystems.libsignal.util.guava.Optional
import org.whispersystems.signalservice.api.crypto.*
import org.whispersystems.signalservice.api.messages.multidevice.*
import org.whispersystems.signalservice.internal.util.Util
import java.io.*
import java.security.Security

class SyncTest
{
	
	@Test
	fun groupStream()
	{
		val arrayOut = ByteArrayOutputStream()
		
		val (group1, group2) = writeGroups(arrayOut)
		val (read1, read2) = readGroups(ByteArrayInputStream(arrayOut.toByteArray()))
		
		assertEquals("Group 1 is not the same", String(group1.id), String(read1.id))
		assertEquals("Group 2 is not the same", String(group2.id), String(read2.id))
	}
	
	@Test
	fun contactStream()
	{
		val arrayOut = ByteArrayOutputStream()
		
		val (contact1, contact2) = writeContacts(arrayOut)
		val (read1, read2) = readContacts(ByteArrayInputStream(arrayOut.toByteArray()))
		
		assertEquals("Contact 1 is not the same", contact1.number, read1.number)
		assertEquals("Contact 2 is not the same", contact2.number, read2.number)
	}
	
	@Test
	fun encryptStream()
	{
		val tmpFile = File.createTempFile("mastest", null)
		tmpFile.deleteOnExit()
		DirtyHacks.removeCryptographyRestrictions()
		Security.addProvider(BouncyCastleProvider())
		val key = ByteArray(64)
		secureRandom.nextBytes(key)
		val data = ByteArray(512)
		secureRandom.nextBytes(data)
		val cipherOut = AttachmentCipherOutputStream(key, tmpFile.outputStream())
		cipherOut.write(data, 0, data.size / 2)
		cipherOut.write(data, data.size / 2, data.size / 2)
		cipherOut.flush()
		val cipherIn = AttachmentCipherInputStream(tmpFile, key, Optional.absent())
		val read = ByteArray(data.size)
		Util.readFully(cipherIn, read)
		println("Data length: ${data.size}, Read length: ${read.size}")
		assertEquals("Stream does not encrypt/decrypt equally", String(data), String(read))
	}
	
	@Test
	fun combined()
	{
		val tmpFile = File.createTempFile("mastest", null)
		tmpFile.deleteOnExit()
		DirtyHacks.removeCryptographyRestrictions()
		Security.addProvider(BouncyCastleProvider())
		val key = ByteArray(64)
		secureRandom.nextBytes(key)
		val cipherOut = AttachmentCipherOutputStream(key, tmpFile.outputStream())
		val tmpOut = ByteArrayOutputStream()
		val (contact1, contact2) = writeContacts(tmpOut)
		cipherOut.write(tmpOut.toByteArray())
		cipherOut.flush()
		val cipherIn = AttachmentCipherInputStream(tmpFile, key, Optional.absent())
		val out2 = ByteArrayOutputStream()
		Util.copy(cipherIn, out2)
		val tmpIn = ByteArrayInputStream(out2.toByteArray())
		val (read1, read2) = readContacts(tmpIn)
		
		assertEquals("Contact 1 is not the same", contact1.number, read1.number)
		assertEquals("Contact 2 is not the same", contact2.number, read2.number)
	}
	
	private fun writeContacts(out : OutputStream) : Pair<DeviceContact, DeviceContact>
	{
		val contact1 = DeviceContact("+49123456789", Optional.absent(), Optional.absent(), Optional.absent(),
				Optional.absent())
		val contact2 = DeviceContact("+49987654321", Optional.absent(), Optional.absent(), Optional.absent(),
				Optional.absent())
		
		val outputStream = DeviceContactsOutputStream(out)
		outputStream.write(contact1)
		outputStream.write(contact2)
		out.flush()
		//out.close()
		return contact1 to contact2
	}
	
	private fun readContacts(input : InputStream) : Pair<DeviceContact, DeviceContact>
	{
		val inputStream = DeviceContactsInputStream(input)
		val read1 = inputStream.read()
		val read2 = inputStream.read()
		input.close()
		return read1 to read2
	}
	
	private fun writeGroups(out : OutputStream) : Pair<DeviceGroup, DeviceGroup>
	{
		val id1 = Util.getSecretBytes(16)
		val id2 = Util.getSecretBytes(16)
		val group1 = DeviceGroup(id1, Optional.of("Name1"), listOf("+49123456789", "+49987654321"), Optional.absent(),
				true)
		val group2 = DeviceGroup(id2, Optional.of("Name2"), listOf("+49123456789", "+49987654321"), Optional.absent(),
				true)
		val outer = DeviceGroupsOutputStream(out)
		outer.write(group1)
		outer.write(group2)
		out.flush()
		out.close()
		return group1 to group2
	}
	
	private fun readGroups(input : InputStream) : Pair<DeviceGroup, DeviceGroup>
	{
		val inter = DeviceGroupsInputStream(input)
		val read1 = inter.read()
		val read2 = inter.read()
		input.close()
		return read1 to read2
	}
}