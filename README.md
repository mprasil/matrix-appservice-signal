# matrix-appservice-signal [![GitLab CI Build Status](https://gitlab.com/mextrix/matrix-appservice-signal/badges/master/build.svg)](https://gitlab.com/mextrix/matrix-appservice-signal/pipelines) [![License](https://img.shields.io/badge/license-GPL--3.0-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

Bridge between a Matrix server and Signal by OpenWhispersystems

## Information

This project aims to provide a bridge for Matrix users to communicate with Signal users. However, they need to have a working Signal account to use this bridge. Users can link their accounts through a special room which they can use as command console. The bridge then acts as a normal Signal desktop client which encrypts messages in the Matrix network and in Signal. Unfortunately, complete End-to-End-Encryption **is not possible** because Matrix encryption is different from Signal encryption. Messages are immediately transmitted to the other service.

## Building and Running
To run this bridge you either have to have a valid configuration located at ```/etc/matrix-appservice-signal.yml``` or you can specify a path through command line arguments: ```<path to config>```. You can use the default config supplied in this repo for testing purposes by using our docker image: ```as-config.yaml```

## Link your account
Invite ```signal``` and type ```register```. Scan the QR code with your Signal app and enjoy talking to your friends!

## Configuration
You can find an annotated example in _configuration/_.

## Used libraries

- [exposed](https://github.com/JetBrains/Exposed) - a kotlin sql framework
- [libsignal-service-java](https://github.com/WhisperSystems/libsignal-service-java) - offers interfaces to establish a working chat session to the Signal servers
- [logback](https://logback.qos.ch/) and [slf4j](https://www.slf4j.org/) for logging
- [matrix-client](https://gitlab.com/mextrix/matrix-client) - simplifies usage of Matrix APIs
- [spark](http://sparkjava.com/) - a web framework
