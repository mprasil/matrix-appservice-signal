# The Matrix AppService API

When registering an AS with your HS (e.g. Synapse), you create two tokens, one to authenticate the AS to
the HS and one the other way round. Next, you reserve a localpart for the AS as well as as many userids,
roomids and room aliases you want defined by one or more regexes. For more information see the
[AS registration spec](https://matrix.org/docs/spec/application_service/unstable.html#registration).

## Contacting the HS from the AS

The HS must grant the AS full control over all namespaces that it controls. This is done by specifying
the token that you created before as the `access_token`. In addition, to act like one of the users that
the AS controls, it is sufficient to use the AS token in combination with the `user_id` field. This can
all be done using the Matrix Client library:

```kotlin
val client = Client(MatrixId("namespace_abc", "localhost"), HomeServer("localhost", "http://localhost:8008/"))
client.token = "Phieyi2EP9ahthah3ahs0guJaicohfiexoh2nuTue0uewooLoh" // replace this with your token
```

## Contacting the AS from the HS

To contact the HS for example to query users and rooms or to push events, the HS has to expose a public api
accessible via http from the HS. The following endpoints are defined:

### `PUT /transactions/:txnId`

A JSON Array containg a batch of events. To tell the HS that the AS has received these events, it must return
the status code 200 with an empty JSON Object (`{}`) in the body.

### `GET /users/:userId`

This is called by the HS to find out if a certain user exists. If it doesn't exist the AS may create it.

To tell the HS that the user exists or was created (it must be registered with the HS before sending the
response!) return the status code 200 with an empty JSON Object (`{}`) in the body.

To tell the HS that the user doesn't exist return the status code 404 with the matrix error response JSON
(`errcode` and `error`) in the body.

### `GET /rooms/:roomAlias`

This is the same as the `/users/:userId` appart that it searches for rooms instead of users
